from subprocess import Popen
from toml import load, dump

EXECUTABLE_MAP = {
    "Dom": "StrokeController.exe",
    "sub": "StrokeControl.exe"
}

def watch():
    while True:
        try:
            with Popen([load("launcher.conf")["executable"]]) as proc:
                proc.communicate()
                return
        except KeyError:
            original = load("launcher.conf")
            original["executable"] = EXECUTABLE_MAP[
                input("Are you a Dom/sub? [Dom/sub]: ")
            ]
            conf = open("launcher.conf", "w")
            dump(original, conf)
            conf.close()