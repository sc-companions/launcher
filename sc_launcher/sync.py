from zipfile import ZipFile
from hashlib import sha256
from base64 import b64decode
from time import sleep
from os import walk, path, mkdir
from typing import Tuple

from toml import load, dump
from requests import get, put, delete, Response

SYNC_SERVER = "https://sc-sync.herokuapp.com/"

ASSET_FOLDER_MAP = {
    "dbase": "Database",
    "imgs": "Images",
    "intake": "Intake",
    "log": "log.txt"
}

def load_data() -> Tuple[str, str]:
    try:
        print("Getting Session...")
        sessid = _get_storage()
        print("Waiting for lock...")
        key = _get_lock(sessid)
        print("Downloading data...")
        _get_data(sessid, key)
        return sessid, key
    except:
        print("Cleaning up lock after crash...")
        if "key" in dir():
            _release_lock(sessid, key)
        raise


def sync(sessid:str, key:str):
    print("Uploading data...")
    _put_data(sessid, key)
    print("Releasing lock...")
    _release_lock(sessid, key)


def _get_storage() -> str:
    while True:
        try:
            with open("launcher.conf", "r") as conf:
                return load(conf)["sessid"]
        except (TypeError, IOError):
            pass
        sessid = get(SYNC_SERVER + "create_storage").text
        with open("launcher.conf", "w") as conf:
            dump({"sessid": sessid}, conf)

def _get_lock(sessid:str) -> str:
    while True:
        result = put(SYNC_SERVER + f"{sessid}/lock").json()
        if result["success"]:
            return result["key"]
        sleep(10)


def _get_data(sessid:str, key:str):
    for asset in ["dbase", "imgs", "intake", "log"]:
        if not path.exists(".cache"):
            mkdir(".cache")
        try:
            with open(f".cache/{asset}.zip", "rb") as f:
                d = f.read()
        except (IOError, FileNotFoundError):
            d = b""
        res:Response = get(
            SYNC_SERVER + f"{sessid}/{asset}",
            auth=("client", key),
            json={
                "hash": sha256(d).digest().hex()
            }
        )
        if not res.json()["modified"]:
            continue
        if asset == "imgs":
            original = load("launcher.conf")
            conf = open("launcher.conf", "w")
            original["sync_imgs"] = True
            dump(original, conf)
            conf.close()
        with open(f".cache/{asset}.zip", "wb") as f:
            f.write(b64decode(res.json()["data"]))
        with ZipFile(f".cache/{asset}.zip") as zip:
            zip.extractall()


def _put_data(sessid:str, key:str):
    for asset in ["dbase", "imgs", "intake", "log"]:
        if asset == "imgs" and not load("launcher.conf").get("sync_imgs", False):
            continue
        with ZipFile(f".cache/{asset}.zip", "w") as zip:
            if path.isdir(ASSET_FOLDER_MAP[asset]):
                for root, dirs, files in walk(ASSET_FOLDER_MAP[asset]):
                    for f in files:
                        zip.write(path.join(root, f))
            else:
                zip.write(ASSET_FOLDER_MAP[asset])
        with open(f".cache/{asset}.zip", "rb") as pkg:
            data = pkg.read()
        put(
            SYNC_SERVER + f"{sessid}/{asset}",
            auth=("client", key),
            data=data
        )


def _release_lock(sessid:str, key:str):
    delete(
        SYNC_SERVER + f"{sessid}/lock",
        auth=("client", key)
    )