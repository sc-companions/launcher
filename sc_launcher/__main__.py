from sys import exit

try:
    from . import main
except:
    from sc_launcher import main

exit(main())