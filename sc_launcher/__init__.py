from .sync import load_data, sync, _release_lock
from .watch import watch

def main():
    try:
        print("Loading data...")
        sessid, key = load_data()
        print("Starting app...")
        watch()
        print("Updating data...")
        sync(sessid, key)
        print("Exitting...")
    except:
        print("Cleaning up lock after crash...")
        if "key" in dir():
            _release_lock(sessid, key)
        raise
    return 0